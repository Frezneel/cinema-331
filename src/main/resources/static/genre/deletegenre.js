$(document).ready(function() {
	getGenreById();
})

function getGenreById() {
	var id = $("#delGenreId").val();
	$.ajax({
		url: "/api/genre/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#kodeDel").val(data.kd_genre);
			$("#namaDel").val(data.nm_genre);
		}
	})
}

$("#delGenreBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#delGenreBtnHapus").click(function() {
	var id = $("#delGenreId").val();
	$.ajax({
		url : "/api/genre/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
		    alert("Berhasil")
            $(".modal").modal("hide")
            getAllGenre();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})