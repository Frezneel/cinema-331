$(document).ready(function() {
	getGenreById();
})

function getGenreById(){
    var id = $("#editGenreId").val();
    	$.ajax({
    		url: "/api/genre/" + id,
    		type: "GET",
    		contentType: "application/json",
    		success: function(data) {
    			$("#kodeEditInput").val(data.kd_genre);
    			$("#namaEditInput").val(data.nm_genre);
    		}
    	})
}

$("#editGenreBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#editGenreBtnBuat").click(function() {
	var id = $("#editGenreId").val();
	var name = $("#namaEditInput").val();
	if (name == "") {
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	var obj = {};
	obj.nm_genre = name;
	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/genre/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
            $(".modal").modal("hide")
            getAllGenre();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})