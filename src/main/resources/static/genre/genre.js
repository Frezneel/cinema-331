function getAllGenre(){
    $("#genreTable").html(
    `<thead>
        <tr>
            <th>Kode Genre</th>
            <th>Nama Genre</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody id="genreTBody"></tbody>
    `
    );

    $.ajax({
        url : "/api/genre",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                $("#genreTBody").append(
                `
                <tr>
                    <td>${data[i].kd_genre}</td>
                    <td>${data[i].nm_genre}</td>
                    <td>
                        <button value="${data[i].kd_genre}" onClick="editGenre(this.value)" class="btn btn-warning">
                            <i class="bi-pencil-square"></i>
                        </button>
                        <button value="${data[i].kd_genre}" onClick="deleteGenre(this.value)" class="btn btn-danger">
                            <i class="bi-trash"></i>
                        </button>
                    </td>
                </tr>
                `
                )
            }
        }
    });
}

$("#addButton").click(function(){
    $.ajax({
        url: "/genre/addgenre",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Buat data Genre baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function editGenre(id){
    $.ajax({
        url: "/genre/editgenre/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Edit Genre");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function deleteGenre(id){
    $.ajax({
        url: "/genre/deletegenre/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Delete Genre");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

$(document).ready(function(){
    getAllGenre();
})