$("#btnAddGenreBatal").click(function(){
    $(".modal").modal("hide")
})

$("#btnAddGenreBuat").click(function(){
    var nama = $("#namaGenreInput").val();
    if(nama == ""){
        $("#errNamaGenre").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaGenre").text("");
    }

    var obj = {};
    obj.nm_genre = nama;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/genre",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            getAllGenre();
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
    });
})