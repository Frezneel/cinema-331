function getAllArtis(){
	$("#artisTable").html(
		`<thead>
			<tr>
				<th>Kode Artis</th>
				<th>Nama Artis</th>
				<th>Jenis Kelamin</th>
				<th>Bayaran</th>
				<th>Award</th>
				<th>Negara</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="artisTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/artis",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#artisTBody").append(
					`
					<tr>
						<td>${data[i].kd_artis}</td>
						<td>${data[i].nm_artis}</td>
						<td>${data[i].jk}</td>
						<td>${data[i].bayaran}</td>
						<td>${data[i].award}</td>
						<td>${(data[i].negara).nm_negara}</td>
                        <td>
                            <button value="${data[i].kd_artis}" onClick="editArtis(this.value)" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].kd_artis}" onClick="deleteArtis(this.value)" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
					</tr>

					`
				)

			}
		}
	});
}

$("#addButton").click(function(){
    $.ajax({
        url: "/artis/addartis",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Buat data Artis baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function editArtis(id){
    $.ajax({
        url: "/artis/editartis/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Edit Artis");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    })
}

function deleteArtis(id){
    $.ajax({
        url: "/artis/deleteartis/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Delete Artis");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

$(document).ready(function(){
	getAllArtis();
})


