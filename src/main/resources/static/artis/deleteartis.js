$(document).ready(function() {
	getArtisById();
})

function getArtisById() {
	var id = $("#delArtisId").val();
	$.ajax({
		url: "/api/artis/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#kodeDel").val(data.kd_artis);
			$("#namaDel").val(data.nm_artis);
			$("#jkDel").val(data.jk);
			$("#bayaranDel").val(data.bayaran);
			$("#awardDel").val(data.award);
			$("#negaraDel").val((data.negara).nm_negara);
		}
	})
}

$("#delArtisBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#delArtisBtnHapus").click(function() {
	var id = $("#delArtisId").val();
	$.ajax({
		url : "/api/artis/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
		    alert("Berhasil")
            $(".modal").modal("hide")
            getAllArtis();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})