$(document).ready(function() {
	getArtisById();
})

function opsiReload(dataJK, dataNegara) {
    var jkSel = document.getElementById("editOpsiJk");
    var negaraSel = document.getElementById("editOpsiNegara");

    if(dataJK == "PRIA"){
        jkSel.options[jkSel.options.length] = new Option("Pria", "PRIA", false, true);
        jkSel.options[jkSel.options.length] = new Option("Wanita", "WANITA");
    }else{
        jkSel.options[jkSel.options.length] = new Option("Pria", "PRIA");
        jkSel.options[jkSel.options.length] = new Option("Wanita", "WANITA", false, true);
    }

    $.ajax({
        url : "/api/negara",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                if(dataNegara == data[i].kd_negara){
                    negaraSel.options[negaraSel.options.length] = new Option(data[i].nm_negara, data[i].kd_negara, false, true);
                }
                else{
                    negaraSel.options[negaraSel.options.length] = new Option(data[i].nm_negara, data[i].kd_negara);
                }
            }
        }
    });

}

function getArtisById(){
    var id = $("#editArtisId").val();
    $.ajax({
        url: "/api/artis/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            $("#kodeEditInput").val(data.kd_artis);
            $("#namaEditInput").val(data.nm_artis);
            $("#editBayaranArtisInput").val(data.bayaran);
            $("#editAwardArtisInput").val(data.award);
            var dataJK = data.jk;
            var dataNegara = data.kd_negara;
            opsiReload(dataJK, dataNegara);
        }
    })
}

$("#editArtisBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#editArtisBtnBuat").click(function() {
	var id = $("#editArtisId").val();
    var nama = $("#namaEditInput").val();
    var jk = $("#editOpsiJk").val();
    var bayaran = $("#editBayaranArtisInput").val();
    var award = $("#editAwardArtisInput").val();
    var negara = $("#editOpsiNegara").val();
    if(nama == ""){
        $("#errNama").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNama").text("");
    }
    if(jk == ""){
        $("#errJk").text("Opsi tidak boleh kosong");
        return
    }else{
        $("#errJk").text("");
    }
    if(bayaran == ""){
        $("#errBayaranArtis").text("Bayaran tidak boleh kosong");
        return
    }else{
        $("#errBayaranArtis").text("");
    }
    if(award == ""){
        $("#errAwardArtis").text("Award tidak boleh kosong");
        return
    }else{
        $("#errAwardArtis").text("");
    }
    if(negara == ""){
        $("#errOpsiNegara").text("Opsi tidak boleh kosong");
        return
    }else{
        $("#errOpsiNegara").text("");
    }

	var obj = {};
	obj.nm_artis = nama;
    obj.jk = jk;
    obj.bayaran = bayaran;
    obj.award = award;
    obj.kd_negara = negara;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/artis/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
            $(".modal").modal("hide")
            getAllArtis();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})