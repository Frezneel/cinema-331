$("#btnAddArtisBatal").click(function(){
    $(".modal").modal("hide")
})

$(document).ready(function(){
	opsiReload();
})


function opsiReload(){
    var jkSel = document.getElementById("opsiJk");
    var negaraSel = document.getElementById("opsiNegara");

    jkSel.options[jkSel.options.length] = new Option("Pria", "PRIA");
    jkSel.options[jkSel.options.length] = new Option("Wanita", "WANITA");

    $.ajax({
        url : "/api/negara",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                negaraSel.options[negaraSel.options.length] = new Option(data[i].nm_negara, data[i].kd_negara);
            }
        }
    });
}


$("#btnAddArtisBuat").click(function(){
    var nama = $("#namaArtisInput").val();
    var jk = $("#opsiJk").val();
    var bayaran = $("#bayaranArtisInput").val();
    var award = $("#awardArtisInput").val();
    var negara = $("#opsiNegara").val();

    if(nama == ""){
        $("#errNamaArtis").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaArtis").text("");
    }
    if(jk == ""){
        $("#errJk").text("Opsi tidak boleh kosong");
        return
    }else{
        $("#errJk").text("");
    }
    if(bayaran == ""){
        $("#errBayaranArtis").text("Bayaran tidak boleh kosong");
        return
    }else{
        $("#errBayaranArtis").text("");
    }
    if(award == ""){
        $("#errAwardArtis").text("Award tidak boleh kosong");
        return
    }else{
        $("#errAwardArtis").text("");
    }
    if(negara == ""){
        $("#errOpsiNegara").text("Opsi tidak boleh kosong");
        return
    }else{
        $("#errOpsiNegara").text("");
    }

    var obj = {};
    obj.nm_artis = nama;
    obj.jk = jk;
    obj.bayaran = bayaran;
    obj.award = award;
    obj.kd_negara = negara;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/artis",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            getAllArtis();
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
    });
})

