$("#btnVerifSewaBatal").click(function(){
    $(".modal").modal("hide")
})

$("#btnVerifSewaProses").click(function(){
   var namaPeminjam = $("#namaPeminjamInput").val();
   if(namaPeminjam == ""){
        $("#errNamaPeminjam").text("Masukkan nama peminjam!");
        return;
   }
   else{
        $("#errNamaPeminjam").text("");
   }

   var objHeader = {};
   var jsonHeader = JSON.stringify(objHeader);

   $.ajax({
        url : "/api/sewaheader",
        type : "POST",
        contentType : "application/json",
        data : jsonHeader,
        success : function(data){
            postData(namaPeminjam,data.id, data.nomor_resi);
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
   });
})

function postData(namaPeminjam, header, resi){
   var banyakFilm = objSimpan.length;
   for(i=0 ; i < objSimpan.length; i++){
        var obj = {};
        obj.jml_hari = objSimpan[i].jml_hari;
        obj.kd_film = objSimpan[i].kd_film;
        obj.total_harga = objSimpan[i].total_harga;
        obj.id_header = header;
        obj.resi = resi;
        obj.nama_penyewa = namaPeminjam;
        var dataSetor = JSON.stringify(obj);
        $.ajax({
            url : "/api/sewadetail",
            type : "POST",
            contentType : "application/json",
            data : dataSetor,
            success : function(data){
            },
            error: function(){
                alert("Terjadi kesalahan");
                return;
            }
        });
   }
   putHeader(header, resi, banyakFilm);
}

function putHeader(header, resi, banyakFilm){
    var objPut = {};
    var hargaTotal = parseInt($("#verifTotalHarga").text());
    var putId = header
    objPut.total_film = banyakFilm;
    objPut.total_harga = hargaTotal;
    objPut.nomor_resi = resi;
    var dataPut = JSON.stringify(objPut);
    console.log(dataPut);
    $.ajax({
        url : "/api/sewaheader/" + putId,
        type : "PUT",
        contentType : "application/json",
        data :dataPut,
        success : function(data){
            $(".modal").modal("hide");
            alert("Data Berhasil Ditambahkan");
            location.reload();
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
    });
}

$(document).ready(function(){
	loadData();
})

function loadData(){
     var verfTotal = $("#totalHarga").text();
     for(i=0 ; i<objSimpan.length; i++){
         $("#verifSewaTBody").append(
            `
            <tr>
                <td>${objSimpan[i].film}</td>
                <td align="center">${objSimpan[i].jml_hari}</td>
                <td>@5000</td>
                <td align="right">${objSimpan[i].total_harga}</td>
            </tr>
            `
        );
    }
    $("#verifTotalHarga").text(verfTotal);
}