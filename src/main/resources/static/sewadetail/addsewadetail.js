$("#btnSewaDetailBatal").click(function(){
    $(".modal").modal("hide")
})
$("#btnSewaDetailTambah").click(function(){
    var optionFilm = document.getElementById("cariDataFilm");
    var film = optionFilm.options[optionFilm.selectedIndex].text;
    var kdfilm = optionFilm.value;
    var jml_hari = $("#jumlahHariSewaInput").val();
    var harga = $("#hargaPerHari").val();
    var total_harga = jml_hari * harga;
    if(kdfilm == ""){
        $("#errCariDataFilm").text("Pilih Opsi!");
        return;
    }
    else{
        $("#errCariDataFilm").text("");
    }
    if(jml_hari == ""){
        $("#errJumlahHariSewa").text("Masukkan jumlah hari!");
        return;
    }
    else{
        $("#errJumlahHariSewa").text("");
    }

    var valTotal = parseInt($("#totalHarga").text());
    var countData = parseInt($("#countData").val()) + 1;
    $("#sewadetailTBody").append(
        `
        <tr>
            <td>${film}</td>
            <td>${jml_hari}</td>
            <td>${harga}</td>
            <td>${total_harga}</td>
        </tr>
        `
    )

    $("#totalHarga").text(valTotal + total_harga);
    $("#countData").val(countData);
    var objData = {}
    objData.film = film;
    objData.kd_film = kdfilm;
    objData.jml_hari = jml_hari;
    objData.total_harga = total_harga;
    objSimpan.push(objData);
// untuk cek data
//    console.log(objSimpan.length);
//    for(i = 0; i<objSimpan.length; i++){
//        console.log(objSimpan[i].film);
//    }

    $(".modal").modal("hide");
})

$(document).ready(function(){
	opsiPilihByReload();
})
var cariOpsi = document.getElementById("cariFilmOpsi");
cariOpsi.onchange = function(){
var opsiData = cariOpsi.value;
if(opsiData != ""){
     $("#opsiDetailTable").html(
      `
      <tr>
          <td>List ${opsiData}</td>
          <td>
              <select name="cariDataOpsi" id="cariDataOpsi">
                  <option value="" selected="selected" >Pilih berdasarkan ${opsiData}</option>
              </select>
              <br>
              <small id="err" class="text-danger"></small>
          </td>
      </tr>
      `
    );
    $("#opsiFilm").html(
        ``);
}else{
    $("#opsiDetailTable").html(
    `
      <tr>
          <td>List</td>
          <td>
              <select name="cariDataOpsi" id="cariDataOpsi">
                  <option value="" selected="selected" >Pilih pilihan sebelumnya terlebih dahulu</option>
              </select>
              <br>
              <small id="err" class="text-danger"></small>
          </td>
      </tr>
    `);
    $("#opsiFilm").html(
    ``)
}
if(opsiData == "artis"){
    $.ajax({
        url : "/api/artis",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            var opsiSel = document.getElementById("cariDataOpsi");
            for(i = 0; i<data.length; i++){
                opsiSel.options[opsiSel.options.length] = new Option(data[i].nm_artis, data[i].kd_artis);
            }
        },
    });
}else if(opsiData == "genre"){
    $.ajax({
        url : "/api/genre",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            var opsiSel = document.getElementById("cariDataOpsi");
            for(i = 0; i<data.length; i++){
                opsiSel.options[opsiSel.options.length] = new Option(data[i].nm_genre, data[i].kd_genre);
            }
        },
    });
}else if(opsiData == "negara"){
    $.ajax({
        url : "/api/negara",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            var opsiSel = document.getElementById("cariDataOpsi");
            for(i = 0; i<data.length; i++){
                opsiSel.options[opsiSel.options.length] = new Option(data[i].nm_negara, data[i].kd_negara);
            }
        },
    });
}else if(opsiData == "produser"){
    $.ajax({
        url : "/api/produser",
        type : "GET",
        contentType : "application/json",
        success : function(data){
            var opsiSel = document.getElementById("cariDataOpsi");
            for(i = 0; i < data.length; i++){
                opsiSel.options[opsiSel.options.length] = new Option(data[i].nm_produser, data[i].kd_produser);
            }
        },
    });
}

var cariOpsiFilm = document.getElementById("cariDataOpsi");
cariOpsiFilm.onchange = function(){
  var cekCariOpsiFilm = cariOpsiFilm.value;
    if(cekCariOpsiFilm != "" && opsiData != ""){
        $("#opsiFilm").html(
          `
          <tr>
              <td>List Film</td>
              <td>
                  <select name="cariDataFilm" id="cariDataFilm">
                      <option value="" selected="selected" >Pilih Film</option>
                  </select>
                  <br>
                  <small id="errCariDataFilm" class="text-danger"></small>
              </td>
          </tr>
          <tr>
              <td>Jumlah Hari Sewa</td>
              <td>
                  <input id="jumlahHariSewaInput" class="form-control" maxlength="100">
                  <small id="errJumlahHariSewa" class="text-danger"></small>
              </td>
          </tr>
          <tr>
              <td>Harga per-Hari</td>
              <td>
                  <input id="hargaPerHari" class="form-control" value="5000" readonly>
              </td>
          </tr>
          `
        )
        $.ajax({
            url : "/api/film/join/"+opsiData+"/"+cekCariOpsiFilm,
            type : "GET",
            contentType : "application/json",
            success : function(data){
            var opsiFilmData = document.getElementById("cariDataFilm");
                for(i = 0; i<data.length; i++){
                    opsiFilmData.options[opsiFilmData.options.length] = new Option(data[i].nm_film, data[i].kd_film);
                }
            },
        });
    }
    else{
        $("#opsiFilm").html(
        `
          <tr>
              <td>List Film</td>
              <td>
                  <select name="cariDataFilm" id="cariDataFilm">
                      <option value="" selected="selected" >Pilih Film</option>
                  </select>
                  <br>
                  <small id="err" class="text-danger"></small>
              </td>
          </tr>
        `)
    }
  }
}

function opsiPilihByReload(){
   var cariOpsi = document.getElementById("cariFilmOpsi");
   cariOpsi.options[cariOpsi.options.length] = new Option("Artis", "artis");
   cariOpsi.options[cariOpsi.options.length] = new Option("Genre", "genre");
   cariOpsi.options[cariOpsi.options.length] = new Option("Negara", "negara");
   cariOpsi.options[cariOpsi.options.length] = new Option("Produser", "produser");
}



