$("#btnHeaderSewaClose").click(function(){
    $(".modal").modal("hide")
})

$(document).ready(function(){
	loadData();
})

function loadData(){
    var id = $("#editDetailHeaderId").val();
    $.ajax({
        url: "/api/sewadetail/header/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            var totalHarga = 0;
            $("#headerNamaPenyewa").val(data[0].nama_penyewa);
            $("#headerHariTransaksi").val(data[0].datetransaksi);
            for(i=0 ; i<data.length; i++){
                totalHarga += data[i].total_harga;
                 $("#headerSewaTBody").append(
                    `
                    <tr>
                        <td>${(data[i].film).nm_film}</td>
                        <td align="center">${data[i].jml_hari}</td>
                        <td>@5000</td>
                        <td align="right">${data[i].total_harga}</td>
                    </tr>
                    `
                );
            }
            $("#headerTotalHarga").text(totalHarga);
        }
    })
}

