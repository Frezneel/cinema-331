$(document).ready(function() {
	getAllSewaHeader();
})

function infoHeader(id){
    $.ajax({
        url: "/sewaheader/detail/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Info Detail");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    })
}

function getAllSewaHeader(){
    $("#sewaheaderTable").html(
        `<thead>
            <tr>
                <th>Id Header</th>
                <th>Nomor Resi</th>
                <th>Total Film</th>
                <th>Total Harga</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="sewaheaderTBody"></tbody>
        `
    );
    $.ajax({
        url : "/api/sewaheader",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                $("#sewaheaderTBody").append(
                    `
                    <tr>
                        <td>${data[i].id}</td>
                        <td>${data[i].nomor_resi}</td>
                        <td>${data[i].total_film}</td>
                        <td>${data[i].total_harga}</td>
                        <td>
                            <button value="${data[i].id}" onClick="infoHeader(this.value)" class="btn btn-primary">
                                <i class="bi bi-info-square"></i>
                            </button>
                        </td>
                    </tr>

                    `
                )
            }
        }
    });
}

