$(document).ready(function() {
	getNegaraById();
})

function getNegaraById() {
	var id = $("#delNegaraId").val();
	$.ajax({
		url: "/api/negara/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    $("#kodeDel").val(data.kd_negara);
			$("#namaDel").val(data.nm_negara);
		}
	})
}

$("#delNegaraBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#delNegaraBtnHapus").click(function() {
	var id = $("#delNegaraId").val();
	$.ajax({
		url : "/api/negara/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
		    alert("Berhasil")
            $(".modal").modal("hide")
            getAllNegara();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})