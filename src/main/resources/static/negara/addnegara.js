$("#btnAddNegaraBatal").click(function(){
    $(".modal").modal("hide")
})

$("#btnAddNegaraBuat").click(function(){
    var nama = $("#namaNegaraInput").val();
    if(nama == ""){
        $("#errNamaNegara").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaNegara").text("");
    }

    var obj = {};
    obj.nm_negara = nama;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/negara",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            getAllNegara();
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
    });
})