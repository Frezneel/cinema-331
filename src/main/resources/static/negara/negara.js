function getAllNegara(){
	$("#negaraTable").html(
		`<thead>
			<tr>
				<th>Kode Negara</th>
				<th>Nama Negara</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="negaraTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/negara",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#negaraTBody").append(
					`
					<tr>
						<td>${data[i].kd_negara}</td>
						<td>${data[i].nm_negara}</td>
						<td>
						    <button value="${data[i].kd_negara}" onClick="editNegara(this.value)" class="btn btn-warning">
						        <i class="bi-pencil-square"></i>
						    </button>
						    <button value="${data[i].kd_negara}" onClick="deleteNegara(this.value)" class="btn btn-danger">
						        <i class="bi-trash"></i>
						    </button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addButton").click(function(){
    $.ajax({
        url: "/negara/addnegara",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Buat data Negara baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function editNegara(id){
    $.ajax({
        url: "/negara/editnegara/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Edit Negara");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

function deleteNegara(id){
    $.ajax({
        url: "/negara/deletenegara/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Delete Negara");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

$(document).ready(function(){
	getAllNegara();
})


