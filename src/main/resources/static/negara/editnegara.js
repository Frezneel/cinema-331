$(document).ready(function() {
	getNegaraById();
})

function getNegaraById(){
    var id = $("#editNegaraId").val();
    $.ajax({
        url: "/api/negara/" + id,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            $("#kodeEditInput").val(data.kd_negara);
            $("#namaEditInput").val(data.nm_negara);
        }
    })
}

$("#editNegaraBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#editNegaraBtnBuat").click(function() {
	var id = $("#editNegaraId").val();
	var name = $("#namaEditInput").val();
	if (name == "") {
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	var obj = {};
	obj.nm_negara = name;
	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/negara/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
            $(".modal").modal("hide")
            getAllNegara();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})