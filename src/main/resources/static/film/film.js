function getAllFilm(){
	$("#filmTable").html(
		`<thead>
			<tr>
				<th>Kode Film</th>
				<th>Nama Film</th>
				<th>Genre</th>
				<th>Artis</th>
				<th>Produser</th>
				<th>Pendapatan</th>
				<th>Nominasi</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="filmTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/film",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#filmTBody").append(
					`
					<tr>
						<td>${data[i].kd_film}</td>
						<td>${data[i].nm_film}</td>
						<td>${(data[i].genre).nm_genre}</td>
						<td>${(data[i].artis).nm_artis}</td>
						<td>${(data[i].produser).nm_produser}</td>
						<td>${data[i].pendapatan}</td>
						<td>${data[i].nominasi}</td>
                        <td>
                            <button value="${data[i].kd_film}" onClick="editFilm(this.value)" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].kd_film}" onClick="deleteFilm(this.value)" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
					</tr>
					`
				)
			}
		}
	});
}

$("#addButton").click(function(){
    $.ajax({
        url: "/film/addfilm",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Buat data Film baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function editFilm(id){
    $.ajax({
        url: "/film/editfilm/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Edit Film");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    })
}

function deleteFilm(id){
    $.ajax({
        url: "/film/deletefilm/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Delete Film");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

$(document).ready(function(){
	getAllFilm();
})


