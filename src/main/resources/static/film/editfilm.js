
$(document).ready(function(){
	getFilmById();
})


function opsiEditReload(dataGenre, dataArtis, dataProduser){
    var genreSel = document.getElementById("editOpsiGenre");
    var artisSel = document.getElementById("editOpsiArtis");
    var produserSel = document.getElementById("editOpsiProduser");

    $.ajax({
        url : "/api/genre",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                if(dataGenre == data[i].kd_genre){
                    genreSel.options[genreSel.options.length] = new Option(data[i].nm_genre, data[i].kd_genre, false, true);
                }
                else{
                    genreSel.options[genreSel.options.length] = new Option(data[i].nm_genre, data[i].kd_genre);
                }
            }
        }
    });
    $.ajax({
        url : "/api/artis",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                if(dataArtis == data[i].kd_artis){
                    artisSel.options[artisSel.options.length] = new Option(data[i].nm_artis, data[i].kd_artis, false, true);
                }
                else{
                    artisSel.options[artisSel.options.length] = new Option(data[i].nm_artis, data[i].kd_artis);
                }
            }
        }
    });
    $.ajax({
        url : "/api/produser",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                if(dataProduser == data[i].kd_produser){
                    produserSel.options[produserSel.options.length] = new Option(data[i].nm_produser, data[i].kd_produser, false, true);
                }
                else{
                    produserSel.options[produserSel.options.length] = new Option(data[i].nm_produser, data[i].kd_produser);
                }
            }
        }
    });
}

function getFilmById(){
    var id = $("#editFilmId").val();
    	$.ajax({
    		url: "/api/film/" + id,
    		type: "GET",
    		contentType: "application/json",
    		success: function(data) {
    			$("#kodeEditInput").val(data.kd_film);
    			$("#editNamaFilmInput").val(data.nm_film);
    			$("#editPendapatanFilmInput").val(data.pendapatan);
    			$("#editNominasiFilmInput").val(data.nominasi);
    			var dataGenre = data.kd_genre;
    			var dataArtis = data.kd_artis;
    			var dataProduser = data.kd_produser;
    			opsiEditReload(dataGenre, dataArtis, dataProduser);
    		}
    	})
}

$("#editFilmBtnBatal").click(function(){
    $(".modal").modal("hide")
})

$("#editFilmBtnBuat").click(function(){
    var id = $("#editFilmId").val();
    var nama = $("#editNamaFilmInput").val();
    var genre = $("#editOpsiGenre").val();
    var artis = $("#editOpsiArtis").val();
    var produser = $("#editOpsiProduser").val();
    var pendapatan = $("#editPendapatanFilmInput").val();
    var nominasi = $("#editNominasiFilmInput").val();

    if(nama == ""){
        $("#errNamaFilm").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaFilm").text("");
    }
    if(genre == ""){
        $("#errOpsiGenre").text("Opsi Genre tidak boleh kosong");
        return
    }else{
        $("#errOpsiGenre").text("");
    }
    if(artis == ""){
        $("#errOpsiArtis").text("Opsi Artis tidak boleh kosong");
        return
    }else{
        $("#errOpsiArtis").text("");
    }
    if(produser == ""){
        $("#errOpsiProduser").text("Opsi Produser tidak boleh kosong");
        return
    }else{
        $("#errOpsiProduser").text("");
    }
    if(pendapatan == ""){
        $("#errPendapatanFilm").text("Pendapatan tidak boleh kosong");
        return
    }else{
        $("#errPendapatanFilm").text("");
    }
    if(nominasi == ""){
        $("#errNominasiFilm").text("Nominasi tidak boleh kosong");
        return
    }else{
        $("#errNominasiFilm").text("");
    }

    var obj = {};
    obj.nm_film = nama;
    obj.kd_genre = genre;
    obj.kd_artis = artis;
    obj.kd_produser = produser;
    obj.pendapatan = pendapatan;
    obj.nominasi = nominasi;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/film/" + id,
        type : "PUT",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            getAllFilm();
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
    });
})

