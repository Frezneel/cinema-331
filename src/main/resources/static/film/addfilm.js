$("#btnAddFilmBatal").click(function(){
    $(".modal").modal("hide")
})

$(document).ready(function(){
	opsiReload();
})


function opsiReload(){
    var genreSel = document.getElementById("opsiGenre");
    var artisSel = document.getElementById("opsiArtis");
    var produserSel = document.getElementById("opsiProduser");

    $.ajax({
        url : "/api/genre",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                genreSel.options[genreSel.options.length] = new Option(data[i].nm_genre, data[i].kd_genre);
            }
        }
    });
    $.ajax({
        url : "/api/artis",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                artisSel.options[artisSel.options.length] = new Option(data[i].nm_artis, data[i].kd_artis);
            }
        }
    });
    $.ajax({
        url : "/api/produser",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                produserSel.options[produserSel.options.length] = new Option(data[i].nm_produser, data[i].kd_produser);
            }
        }
    });


}


$("#btnAddFilmBuat").click(function(){
    var nama = $("#namaFilmInput").val();
    var genre = $("#opsiGenre").val();
    var artis = $("#opsiArtis").val();
    var produser = $("#opsiProduser").val();
    var pendapatan = $("#pendapatanFilmInput").val();
    var nominasi = $("#nominasiFilmInput").val();

    if(nama == ""){
        $("#errNamaFilm").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaFilm").text("");
    }
    if(genre == ""){
        $("#errOpsiGenre").text("Opsi Genre tidak boleh kosong");
        return
    }else{
        $("#errOpsiGenre").text("");
    }
    if(artis == ""){
        $("#errOpsiArtis").text("Opsi Artis tidak boleh kosong");
        return
    }else{
        $("#errOpsiArtis").text("");
    }
    if(produser == ""){
        $("#errOpsiProduser").text("Opsi Produser tidak boleh kosong");
        return
    }else{
        $("#errOpsiProduser").text("");
    }
    if(pendapatan == ""){
        $("#errPendapatanFilm").text("Pendapatan tidak boleh kosong");
        return
    }else{
        $("#errPendapatanFilm").text("");
    }
    if(nominasi == ""){
        $("#errNominasiFilm").text("Nominasi tidak boleh kosong");
        return
    }else{
        $("#errNominasiFilm").text("");
    }

    var obj = {};
    obj.nm_film = nama;
    obj.kd_genre = genre;
    obj.kd_artis = artis;
    obj.kd_produser = produser;
    obj.pendapatan = pendapatan;
    obj.nominasi = nominasi;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/film",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            getAllFilm();
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
    });
})

