$(document).ready(function() {
	getFilmById();
})

function getFilmById() {
	var id = $("#delFilmId").val();
	$.ajax({
		url: "/api/film/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#kodeDel").val(data.kd_film);
			$("#namaDel").val(data.nm_film);
			$("#genreDel").val((data.genre).nm_genre);
			$("#artisDel").val((data.artis).nm_artis);
			$("#produserDel").val((data.produser).nm_produser);
			$("#pendapatanDel").val(data.pendapatan);
			$("#nominasiDel").val(data.nominasi);
		}
	})
}

$("#delFilmBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#delFilmBtnHapus").click(function() {
	var id = $("#delFilmId").val();
	$.ajax({
		url : "/api/film/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
		    alert("Berhasil")
            $(".modal").modal("hide")
            getAllFilm();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})