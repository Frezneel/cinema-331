$(document).ready(function() {
	getProduserById();
})

function getProduserById() {
	var id = $("#delProduserId").val();
	$.ajax({
		url: "/api/produser/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#kodeDel").val(data.kd_produser);
			$("#namaDel").val(data.nm_produser);
			$("#interDel").val(data.international == true ? "IYA" : "TIDAK");
		}
	})
}

$("#delProduserBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#delProduserBtnHapus").click(function() {
	var id = $("#delProduserId").val();
	$.ajax({
		url : "/api/produser/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
		    alert("Berhasil")
            $(".modal").modal("hide")
            getAllProduser();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})