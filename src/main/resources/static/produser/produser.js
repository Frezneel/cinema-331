function getAllProduser(){
	$("#produserTable").html(
		`<thead>
			<tr>
				<th>Kode Produser</th>
				<th>Nama Produser</th>
				<th>International</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="produserTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/produser",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#produserTBody").append(
					`
					<tr>
						<td>${data[i].kd_produser}</td>
						<td>${data[i].nm_produser}</td>
						<td>${data[i].international == true ? "IYA" : "TIDAK"}</td>
                        <td>
                            <button value="${data[i].kd_produser}" onClick="editProduser(this.value)" class="btn btn-warning">
                                <i class="bi-pencil-square"></i>
                            </button>
                            <button value="${data[i].kd_produser}" onClick="deleteProduser(this.value)" class="btn btn-danger">
                                <i class="bi-trash"></i>
                            </button>
                        </td>
					</tr>
					`
				)

			}
		}
	});
}

$("#addButton").click(function(){
    $.ajax({
        url: "/produser/addproduser",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Buat data Produser baru");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
})

function deleteProduser(id){
    $.ajax({
            url: "/produser/deleteproduser/" + id,
            type: "GET",
            contentType: "html",
            success: function(data){
                $(".modal-title").text("Delete Produser");
                $(".modal-body").html(data);
                $(".modal").modal("show");
            }
        });
}

function editProduser(id){
    $.ajax({
        url: "/produser/editproduser/" + id,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Edit Produser");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    })
}

$(document).ready(function(){
	getAllProduser();
})


