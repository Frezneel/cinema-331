$(document).ready(function() {
	getProduserById();
})

function opsiInternational(opsi) {
    var internationalSel = document.getElementById("editInternational");
    if(opsi){
        internationalSel.options[internationalSel.options.length] = new Option("Iya", true, false, true);
        internationalSel.options[internationalSel.options.length] = new Option("Tidak", false);
    }else{
        internationalSel.options[internationalSel.options.length] = new Option("Iya", true);
        internationalSel.options[internationalSel.options.length] = new Option("Tidak", false, false, true);
    }

}

function getProduserById(){
    var id = $("#editProduserId").val();
    	$.ajax({
    		url: "/api/produser/" + id,
    		type: "GET",
    		contentType: "application/json",
    		success: function(data) {
    			$("#kodeEditInput").val(data.kd_produser);
    			$("#namaEditInput").val(data.nm_produser);
    			var opsi = data.international;
    			opsiInternational(opsi);
    		}
    	})
}

$("#editProduserBtnBatal").click(function() {
	$(".modal").modal("hide")
})

$("#editProduserBtnBuat").click(function() {
	var id = $("#editProduserId").val();
	var name = $("#namaEditInput").val();
	var international = $("#editInternational").val();
	if (name == "") {
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}
	if (international == "") {
		$("#errInternational").text("Opsi tidak boleh kosong!");
		return;
	} else {
		$("#errInternational").text("");
	}


	var obj = {};
	obj.nm_produser = name;
	obj.international = international;
	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/produser/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
            $(".modal").modal("hide")
            getAllProduser();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})