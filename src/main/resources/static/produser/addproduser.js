$("#btnAddProduserBatal").click(function(){
    $(".modal").modal("hide")
})

$(document).ready(function(){
	opsiInternational();
})

function opsiInternational() {
    var internationalSel = document.getElementById("international");
    internationalSel.options[internationalSel.options.length] = new Option("Iya", true);
    internationalSel.options[internationalSel.options.length] = new Option("Tidak", false);
}

$("#btnAddProduserBuat").click(function(){
    var nama = $("#namaProduserInput").val();
    var international = $("#international").val();
    if(nama == ""){
        $("#errNamaProduser").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaProduser").text("");
    }
    if(international == ""){
        $("#errInternational").text("Opsi tidak boleh kosong");
        return
    }else{
        $("#errInternational").text("");
    }
    var obj = {};
    obj.nm_produser = nama;
    obj.international = international;

    var myJson = JSON.stringify(obj);

    $.ajax({
        url : "/api/produser",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            $(".modal").modal("hide")
            getAllProduser();
        },
        error: function(){
            alert("Terjadi kesalahan")
        }
    });
})