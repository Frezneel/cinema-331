package com.belajar.cinema.controllers;

import com.belajar.cinema.models.Film;
import com.belajar.cinema.repositores.RepoFilm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiFilmController {
    @Autowired
    private RepoFilm repoFilm;

    // Read all
    @GetMapping("/film")
    public ResponseEntity<List<Film>> getFilmAllData(){
        try {
            List<Film> film = this.repoFilm.findAll();
            return new ResponseEntity<>(film, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    // Read by id
    @GetMapping("/film/{id}")
    public ResponseEntity<List<Film>> getFilmById(@PathVariable Integer id){
        try {
            Optional<Film> film = this.repoFilm.findById(id);
            if (film.isPresent()){
                ResponseEntity response = new ResponseEntity<>(film, HttpStatus.OK);
                return response;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //Artis
    @GetMapping("/film/join/artis/{id}")
    public ResponseEntity<List<Film>> getFilmByArtis(@PathVariable("id") Integer id){
        try {
            List<Film> film = this.repoFilm.joinLeftFilmByKdArtis(id);
            ResponseEntity response = new ResponseEntity<>(film, HttpStatus.OK);
            return response;

        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    //Genre
    @GetMapping("/film/join/genre/{id}")
    public ResponseEntity<List<Film>> getFilmByGenre(@PathVariable("id") Integer id){
        try {
            List<Film> film = this.repoFilm.joinLeftFilmByKdGenre(id);
            ResponseEntity response = new ResponseEntity<>(film, HttpStatus.OK);
            return response;

        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    //Negara
    @GetMapping("/film/join/negara/{id}")
    public ResponseEntity<List<Film>> getFilmByNegara(@PathVariable("id") Integer id){
        try {
            List<Film> film = this.repoFilm.joinLeftFilmByKdNegara(id);
            ResponseEntity response = new ResponseEntity<>(film, HttpStatus.OK);
            return response;

        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    //Produser
    @GetMapping("/film/join/produser/{id}")
    public ResponseEntity<List<Film>> getFilmByProduser(@PathVariable("id") Integer id){
        try {
            List<Film> film = this.repoFilm.joinLeftFilmByKdProduser(id);
            ResponseEntity response = new ResponseEntity<>(film, HttpStatus.OK);
            return response;

        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    // Create data
    @PostMapping("/film")
    public ResponseEntity<Object> saveData(@RequestBody Film film){
        try {
            this.repoFilm.save(film);
            return new ResponseEntity<>(film, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    // Put data
    @PutMapping("/film/{id}")
    public ResponseEntity<Object> updateData(@RequestBody Film film, @PathVariable Integer id){
            Optional<Film> dataFilm = this.repoFilm.findById(id);
            if (dataFilm.isPresent()){
                film.setKd_film(id);
                this.repoFilm.save(film);
                ResponseEntity response = new ResponseEntity<>("Success", HttpStatus.OK);
                return response;
            }else {
                return ResponseEntity.notFound().build();
            }
    }

    // Delete data
    @DeleteMapping("/film/{id}")
    public ResponseEntity<Object> deleteDataById(@PathVariable Integer id){
        this.repoFilm.deleteById(id);
        return new ResponseEntity("Success", HttpStatus.OK);
    }

}
