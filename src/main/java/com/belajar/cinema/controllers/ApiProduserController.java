package com.belajar.cinema.controllers;

import com.belajar.cinema.models.Produser;
import com.belajar.cinema.repositores.RepoProduser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiProduserController {

    @Autowired
    private RepoProduser repoProduser;

    //Get List Semua data
    @GetMapping("/produser")
    public ResponseEntity<List<Produser>> getProduserAllData(){
        try {
            List<Produser> produser = this.repoProduser.FindAllProduserOrderByASC();
            return new ResponseEntity<>(produser, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //Get List by id
    @GetMapping("/produser/{id}")
    public ResponseEntity<List<Produser>> getProduserById(@PathVariable("id") Integer id){
        try {
            Optional<Produser> produser = this.repoProduser.findById(id);
            if (produser.isPresent()){
                ResponseEntity response = new ResponseEntity<>(produser, HttpStatus.OK);
                return response;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //Post Data
    @PostMapping("/produser")
    public ResponseEntity<Object> saveProduser(@RequestBody Produser produser){
        try {
            this.repoProduser.save(produser);
            return new ResponseEntity<>(produser, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/produser/{id}")
    public ResponseEntity<Object> updateProduser(@RequestBody Produser produser, @PathVariable("id") Integer id){
        Optional<Produser> dataProduser = this.repoProduser.findById(id);
        if (dataProduser.isPresent()){
            produser.setKd_produser(id);
            this.repoProduser.save(produser);
            ResponseEntity response = new ResponseEntity<>("Success", HttpStatus.OK);
            return response;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/produser/{id}")
    public ResponseEntity<Object> deleteProduser(@PathVariable("id") Integer id){
        this.repoProduser.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
