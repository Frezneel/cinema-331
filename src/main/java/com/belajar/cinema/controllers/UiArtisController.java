package com.belajar.cinema.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("artis")
public class UiArtisController {
    @RequestMapping("")
    public String artis(){
        return "artis/artis";
    }

    @RequestMapping("addartis")
    public String addArtis(){
        return "artis/addartis";
    }

    @RequestMapping("editartis/{id}")
    public String editArtis(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "artis/editartis";
    }

    @RequestMapping("deleteartis/{id}")
    public String deleteArtis(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "artis/deleteartis";
    }
}
