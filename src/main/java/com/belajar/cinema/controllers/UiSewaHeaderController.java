package com.belajar.cinema.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("sewaheader")
public class UiSewaHeaderController {

    @RequestMapping("")
    public String sewaHeader(){
        return "sewaheader/sewaheader";
    }
    @RequestMapping("detail/{id}")
    public String detailHeader(@PathVariable("id") Long id, Model model){
        model.addAttribute(id);
        return "sewaheader/detailheader";
    }


}
