package com.belajar.cinema.controllers;

import com.belajar.cinema.models.Negara;
import com.belajar.cinema.repositores.RepoNegara;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiNegaraController {
    @Autowired
    private RepoNegara repoNegara;

    // Read All
    @GetMapping("/negara")
    public ResponseEntity<List<Negara>> getAllNegara()
    {
        try {
            List<Negara> negara = this.repoNegara.FindAllNegaraOrderByASC();
            return new ResponseEntity<>(negara, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //Saving data negara
    @PostMapping("/negara")
    public ResponseEntity<Object> saveNegara(@RequestBody Negara negara)
    {
        try {
            this.repoNegara.save(negara);
            return new ResponseEntity<>(negara, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    //get By id
    @GetMapping("/negara/{id}")
    public ResponseEntity<List<Negara>> getNegaraById(@PathVariable("id") Integer id)
    {
        try {
            Optional<Negara> negara = this.repoNegara.findById(id);
            if (negara.isPresent()){
                ResponseEntity response =new ResponseEntity<>(negara, HttpStatus.OK);
                return response;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //Put by id
    @PutMapping("/negara/{id}")
    public ResponseEntity<Object> UpdateNegara (@RequestBody Negara negara, @PathVariable("id") Integer id){
        Optional<Negara> negaraData =this.repoNegara.findById(id);
        if (negaraData.isPresent()){
            negara.setKd_negara(id);
            this.repoNegara.save(negara);
            ResponseEntity response = new ResponseEntity<>("Success", HttpStatus.OK);
            return response;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    //Delete
    @DeleteMapping("/negara/{id}")
    public ResponseEntity<Object> DeleteNegara(@PathVariable ("id") Integer id){
        this.repoNegara.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
