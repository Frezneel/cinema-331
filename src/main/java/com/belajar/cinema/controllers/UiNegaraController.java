package com.belajar.cinema.controllers;

import com.belajar.cinema.models.Negara;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("negara")
public class UiNegaraController {

    @RequestMapping("")
    public String negara(){
        return "negara/negara";
    }

    @RequestMapping("addnegara")
    public String addNegara(){
        return "negara/addnegara";
    }

    @RequestMapping("editnegara/{id}")
    public String editNegara(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "negara/editnegara";
    }

    @RequestMapping("deletenegara/{id}")
    public String deleteNegara(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "negara/deletenegara";
    }
}
