package com.belajar.cinema.controllers;

import com.belajar.cinema.models.SewaDetail;
import com.belajar.cinema.repositores.RepoSewaDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiSewaDetail {

    @Autowired
    private RepoSewaDetail repoSewaDetail;

    @GetMapping("/sewadetail")
    public ResponseEntity<List<SewaDetail>> getAllSewaDetail(){
        try {
            List<SewaDetail> sewaDetails = this.repoSewaDetail.findAll();
            return new ResponseEntity<>(sewaDetails, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/sewadetail/header/{id}")
    public ResponseEntity<List<SewaDetail>> getAllDetailByIdHeader(@PathVariable("id") Long id){
        try {
            List<SewaDetail> sewaDetails = this.repoSewaDetail.getAllDetailByIdHeader(id);
            return new ResponseEntity<>(sewaDetails, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/sewadetail")
    public ResponseEntity<Object> saveSewaDetail(@RequestBody SewaDetail sewaDetail){
        try {
            sewaDetail.setDatetransaksi(new Date());
            this.repoSewaDetail.save(sewaDetail);
            return new ResponseEntity<>(sewaDetail, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Failed",HttpStatus.BAD_REQUEST);
        }
    }
}
