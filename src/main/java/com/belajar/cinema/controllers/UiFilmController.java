package com.belajar.cinema.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("film")
public class UiFilmController {

    @RequestMapping("")
    public String film(){
        return "film/film";
    }

    @RequestMapping("addfilm")
    public String addFilm(){
        return "film/addfilm";
    }

    @RequestMapping("editfilm/{id}")
    public String editFilm(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "film/editfilm";
    }

    @RequestMapping("deletefilm/{id}")
    public String deleteFilm(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "film/deletefilm";
    }

}
