package com.belajar.cinema.controllers;

import com.belajar.cinema.models.SewaDetail;
import com.belajar.cinema.models.SewaHeader;
import com.belajar.cinema.repositores.RepoSewaHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("api")
public class ApiSewaHeader {

    @Autowired
    private RepoSewaHeader repoSewaHeader;

    @GetMapping("/sewaheader")
    public ResponseEntity<List<SewaHeader>> getAllSewaHeader(){
        try {
            List<SewaHeader> sewaHeader = this.repoSewaHeader.findAll();
            return new ResponseEntity<>(sewaHeader, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/sewaheader")
    public ResponseEntity<Object> saveSewaHeader(@RequestBody SewaHeader sewaHeader){
        try {
            Long timeString = System.currentTimeMillis();
            sewaHeader.setNomor_resi(timeString);
            this.repoSewaHeader.save(sewaHeader);
            return new ResponseEntity<>(sewaHeader, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/sewaheader/{id}")
    public ResponseEntity<Object> updateSewaHeader(@RequestBody SewaHeader sewaHeader, @PathVariable("id") Long id){
        Optional<SewaHeader> dataHeader = this.repoSewaHeader.findById(id);
        if (dataHeader.isPresent()){
            sewaHeader.setId(id);
            this.repoSewaHeader.save(sewaHeader);
            ResponseEntity response = new ResponseEntity<>("Success", HttpStatus.OK);
            return response;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

}
