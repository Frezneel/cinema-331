package com.belajar.cinema.controllers;


import com.belajar.cinema.models.Genre;
import com.belajar.cinema.repositores.RepoGenre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiGenreController {

    @Autowired
    private RepoGenre repoGenre;

    //Ambil semua
    @GetMapping("/genre")
    public ResponseEntity<List<Genre>> getAllGenre()
    {
        try {
            List<Genre> genre = this.repoGenre.findAll();
            return new ResponseEntity<>(genre, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //Ambil Berdasarkan Id
    @GetMapping("/genre/{id}")
    public ResponseEntity<List<Genre>> getById(@PathVariable("id") Integer id){
        try {
            Optional<Genre> genre = this.repoGenre.findById(id);
            if (genre.isPresent()){
                ResponseEntity response = new ResponseEntity<>(genre, HttpStatus.OK);
                return response;
            }
            else{
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    //Post Data
    @PostMapping("/genre")
    public ResponseEntity<Object> saveGenre (@RequestBody Genre genre){
        try {
            this.repoGenre.save(genre);
            return new ResponseEntity<>(genre, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST    );
        }
    }

    //Update Data
    @PutMapping("/genre/{id}")
    public ResponseEntity<Object> updateGenre(@RequestBody Genre genre, @PathVariable("id") Integer id){
        Optional<Genre> genreData = this.repoGenre.findById(id);
        if (genreData.isPresent()){
            genre.setKd_genre(id);
            this.repoGenre.save(genre);
            ResponseEntity response = new ResponseEntity<>("Success", HttpStatus.OK);
            return response;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    //Delete Data by id
    @DeleteMapping("/genre/{id}")
    public ResponseEntity<Object> deleteGenreById(@PathVariable("id") Integer id){
        this.repoGenre.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
