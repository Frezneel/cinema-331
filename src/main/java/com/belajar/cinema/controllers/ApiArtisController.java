package com.belajar.cinema.controllers;

import com.belajar.cinema.models.Artis;
import com.belajar.cinema.models.Film;
import com.belajar.cinema.repositores.RepoArtis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiArtisController {
    @Autowired
    private RepoArtis repoArtis;

    //Get All
    @GetMapping("/artis")
    public ResponseEntity<List<Artis>> getAllArtis()
    {
        try {
            List<Artis> artis = this.repoArtis.findAll();
            return new ResponseEntity<>(artis, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    // Ambil berdasarkan id
    @GetMapping("/artis/{id}")
    public ResponseEntity<List<Artis>> getArtisById(@PathVariable("id") Integer id)
    {
        try {
            Optional<Artis> artis = this.repoArtis.findById(id);
            if (artis.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(artis, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/artis")
    public ResponseEntity<Object> saveArtis(@RequestBody Artis artis)
    {
        try {
            this.repoArtis.save(artis);
            return new ResponseEntity<>(artis, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    //Update pada id
    @PutMapping("/artis/{id}")
    public ResponseEntity<Object> updateArtis(@RequestBody Artis artis, @PathVariable("id") Integer id)
    {
        Optional<Artis> artisData = this.repoArtis.findById(id);

        if (artisData.isPresent())
        {
            artis.setKd_artis(id);
            this.repoArtis.save(artis);
            ResponseEntity response = new ResponseEntity<>("Success", HttpStatus.OK);
            return response;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    //Menghapus berdasarkan id
    @DeleteMapping("/artis/{id}")
    public ResponseEntity<Object> deleteArtis(@PathVariable("id") Integer id)
    {
        this.repoArtis.deleteById(id);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
