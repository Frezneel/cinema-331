package com.belajar.cinema.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("sewadetail")
public class UiSewaDetailController {

    @RequestMapping("")
    public String sewaDetail(){
        return "sewadetail/sewadetail";
    }

    @RequestMapping("addsewadetail")
    public String addSewaDetail(){
        return "sewadetail/addsewadetail";
    }

    @RequestMapping("verifsewadetail")
    public String verifSewaDetail(){
        return "sewadetail/verifsewadetail";
    }


}
