package com.belajar.cinema.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("genre")
public class UiGenreController {

    @RequestMapping("")
    public String genre(){
        return "genre/genre";
    }

    @RequestMapping("addgenre")
    public String addGenre(){
        return "genre/addgenre";
    }

    @RequestMapping("editgenre/{id}")
    public String editGenre(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "genre/editgenre";
    }

    @RequestMapping("deletegenre/{id}")
    public String deleteGenre(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "genre/deletegenre";
    }
}
