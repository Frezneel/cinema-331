package com.belajar.cinema.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("produser")
public class UiProduserController {

    @RequestMapping("")
    public String produser(){
        return "produser/produser";
    }

    @RequestMapping("addproduser")
    public String addProduser(){
        return "produser/addproduser";
    }

    @RequestMapping("editproduser/{id}")
    public String editProduser(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "produser/editproduser";
    }

    @RequestMapping("deleteproduser/{id}")
    public String deleteProduser(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id",id);
        return "produser/deleteproduser";
    }
}
