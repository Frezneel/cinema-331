package com.belajar.cinema.models;

import jakarta.persistence.*;

@Entity
@Table(name = "produser")
public class Produser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kd_produser", nullable = false)
    private int kd_produser;
    @Column(name = "nm_produser", nullable = false)
    private String nm_produser;
    @Column(name = "international", nullable = false)
    private boolean international;

    public int getKd_produser() {
        return kd_produser;
    }

    public void setKd_produser(int kd_produser) {
        this.kd_produser = kd_produser;
    }

    public String getNm_produser() {
        return nm_produser;
    }

    public void setNm_produser(String nm_produser) {
        this.nm_produser = nm_produser;
    }

    public boolean isInternational() {
        return international;
    }

    public void setInternational(boolean international) {
        this.international = international;
    }
}
