package com.belajar.cinema.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "sewa_detail")
public class SewaDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "nama_penyewa", nullable = false)
    private String nama_penyewa;

    @ManyToOne
    @JoinColumn(name = "film", insertable = false, updatable = false)
    private Film film;

    @Column(name = "film", nullable = false)
    private int kd_film;

    @Column(name = "jml_hari", nullable = false)
    private int jml_hari;

    @Column(name = "total_harga", nullable = false)
    private long total_harga;
    @Column(name = "resi", nullable = false)
    private long resi;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date datetransaksi;


    @ManyToOne
    @JoinColumn(name = "id_header", updatable = false, insertable = false)
    private SewaHeader sewaHeader;

    @Column(name = "id_header", nullable = false)
    private long id_header;

    public SewaHeader getSewaHeader() {
        return sewaHeader;
    }

    public void setSewaHeader(SewaHeader sewaHeader) {
        this.sewaHeader = sewaHeader;
    }

    public long getId_header() {
        return id_header;
    }

    public void setId_header(long id_header) {
        this.id_header = id_header;
    }

    public long getResi() {
        return resi;
    }

    public void setResi(long resi) {
        this.resi = resi;
    }

    public String getNama_penyewa() {
        return nama_penyewa;
    }

    public void setNama_penyewa(String nama_penyewa) {
        this.nama_penyewa = nama_penyewa;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public int getKd_film() {
        return kd_film;
    }

    public void setKd_film(int kd_film) {
        this.kd_film = kd_film;
    }

    public int getJml_hari() {
        return jml_hari;
    }

    public void setJml_hari(int jml_hari) {
        this.jml_hari = jml_hari;
    }

    public long getTotal_harga() {
        return total_harga;
    }

    public void setTotal_harga(long total_harga) {
        this.total_harga = total_harga;
    }

    public Date getDatetransaksi() {
        return datetransaksi;
    }

    public void setDatetransaksi(Date datetransaksi) {
        this.datetransaksi = datetransaksi;
    }

}
