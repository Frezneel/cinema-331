package com.belajar.cinema.models;

import jakarta.persistence.*;

@Entity
@Table(name = "genre")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kd_genre", nullable = false)
    private int kd_genre;
    @Column(name = "nm_genre", nullable = false)
    private String nm_genre;

    public int getKd_genre() {
        return kd_genre;
    }

    public void setKd_genre(int kd_genre) {
        this.kd_genre = kd_genre;
    }

    public String getNm_genre() {
        return nm_genre;
    }

    public void setNm_genre(String nm_genre) {
        this.nm_genre = nm_genre;
    }
}
