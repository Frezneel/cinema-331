package com.belajar.cinema.models;

import jakarta.persistence.*;

@Entity
@Table(name = "artis")
public class Artis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kd_artis", nullable = false)
    private int kd_artis;
    @Column(name = "nm_artis", nullable = false)
    private String nm_artis;
    @Column(name = "jk", nullable = false)
    private String jk;
    @Column(name = "bayaran", nullable = false)
    private long bayaran;
    @Column(name = "award", nullable = false)
    private int award;

    @OneToOne
    @JoinColumn(name = "negara", insertable = false, updatable = false)
    public Negara negara;
    @Column(name = "negara")
    private int kd_negara;

    public int getKd_artis() {
        return kd_artis;
    }

    public void setKd_artis(int kd_artis) {
        this.kd_artis = kd_artis;
    }

    public String getNm_artis() {
        return nm_artis;
    }

    public void setNm_artis(String nm_artis) {
        this.nm_artis = nm_artis;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public long getBayaran() {
        return bayaran;
    }

    public void setBayaran(long bayaran) {
        this.bayaran = bayaran;
    }

    public int getAward() {
        return award;
    }

    public void setAward(int award) {
        this.award = award;
    }

    public Negara getNegara() {
        return negara;
    }

    public void setNegara(Negara negara) {
        this.negara = negara;
    }

    public int getKd_negara() {
        return kd_negara;
    }

    public void setKd_negara(int kd_negara) {
        this.kd_negara = kd_negara;
    }
}
