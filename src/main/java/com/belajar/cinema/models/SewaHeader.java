package com.belajar.cinema.models;

import jakarta.persistence.*;

@Entity
@Table(name = "sewa_header")
public class SewaHeader {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "total_film", nullable = true)
    private long total_film;

    @Column(name = "total_harga", nullable = true)
    private long total_harga;

    @Column(name = "nomor_resi", nullable = false)
    private long nomor_resi;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTotal_film() {
        return total_film;
    }

    public void setTotal_film(long total_film) {
        this.total_film = total_film;
    }

    public long getTotal_harga() {
        return total_harga;
    }

    public void setTotal_harga(long total_harga) {
        this.total_harga = total_harga;
    }

    public long getNomor_resi() {
        return nomor_resi;
    }

    public void setNomor_resi(long nomor_resi) {
        this.nomor_resi = nomor_resi;
    }
}
