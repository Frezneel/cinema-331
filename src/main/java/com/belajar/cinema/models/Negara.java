package com.belajar.cinema.models;

import jakarta.persistence.*;

@Entity
@Table (name = "negara")
public class Negara {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kd_negara", nullable = false)
    private int kd_negara;
    @Column(name = "nm_negara", nullable = false)
    private String nm_negara;

    public int getKd_negara() {
        return kd_negara;
    }

    public void setKd_negara(int kd_negara) {
        this.kd_negara = kd_negara;
    }

    public String getNm_negara() {
        return nm_negara;
    }

    public void setNm_negara(String nm_negara) {
        this.nm_negara = nm_negara;
    }
}
