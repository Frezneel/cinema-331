package com.belajar.cinema.models;

import jakarta.persistence.*;

@Entity
@Table(name = "film")
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kd_film", nullable = false)
    private int kd_film;
    @Column(name = "nm_film", nullable = false)
    private String nm_film;
    @OneToOne
    @JoinColumn(name = "genre", insertable = false, updatable = false)
    private Genre genre;
    @Column(name = "genre", nullable = false)
    private int kd_genre;
    @OneToOne
    @JoinColumn(name = "artis", insertable = false, updatable = false)
    private Artis artis;
    @Column(name = "artis", nullable = false)
    private int kd_artis;
    @OneToOne
    @JoinColumn(name = "produser", insertable = false, updatable = false)
    private Produser produser;
    @Column(name = "produser", nullable = false)
    private int kd_produser;
    @Column(name = "pendapatan", nullable = false)
    private long pendapatan;
    @Column(name = "nominasi", nullable = false)
    private int nominasi;

    public int getKd_film() {
        return kd_film;
    }

    public void setKd_film(int kd_film) {
        this.kd_film = kd_film;
    }

    public String getNm_film() {
        return nm_film;
    }

    public void setNm_film(String nm_film) {
        this.nm_film = nm_film;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getKd_genre() {
        return kd_genre;
    }

    public void setKd_genre(int kd_genre) {
        this.kd_genre = kd_genre;
    }

    public Artis getArtis() {
        return artis;
    }

    public void setArtis(Artis artis) {
        this.artis = artis;
    }

    public int getKd_artis() {
        return kd_artis;
    }

    public void setKd_artis(int kd_artis) {
        this.kd_artis = kd_artis;
    }

    public Produser getProduser() {
        return produser;
    }

    public void setProduser(Produser produser) {
        this.produser = produser;
    }

    public int getKd_produser() {
        return kd_produser;
    }

    public void setKd_produser(int kd_produser) {
        this.kd_produser = kd_produser;
    }

    public long getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(long pendapatan) {
        this.pendapatan = pendapatan;
    }

    public int getNominasi() {
        return nominasi;
    }

    public void setNominasi(int nominasi) {
        this.nominasi = nominasi;
    }
}
