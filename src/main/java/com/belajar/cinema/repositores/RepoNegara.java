package com.belajar.cinema.repositores;

import com.belajar.cinema.models.Negara;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoNegara extends JpaRepository<Negara, Integer> {
    @Query(value = "SELECT * FROM negara ORDER BY negara ASC", nativeQuery = true)
    List<Negara> FindAllNegaraOrderByASC();
}
