package com.belajar.cinema.repositores;

import com.belajar.cinema.models.Negara;
import com.belajar.cinema.models.SewaHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoSewaHeader extends JpaRepository<SewaHeader, Long> {
    @Query(value = "SELECT sewa_detail.* FROM sewa_header INNER JOIN sewa_detail ON sewa_detail.id_header = sewa_header.id", nativeQuery = true)
    List<Negara> FindAllNegaraOrderByASC();
}
