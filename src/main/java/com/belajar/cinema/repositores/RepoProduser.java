package com.belajar.cinema.repositores;

import com.belajar.cinema.models.Negara;
import com.belajar.cinema.models.Produser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoProduser extends JpaRepository<Produser, Integer> {
    @Query(value = "SELECT * FROM produser ORDER BY produser ASC", nativeQuery = true)
    List<Produser> FindAllProduserOrderByASC();
}
