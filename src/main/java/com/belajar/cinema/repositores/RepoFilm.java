package com.belajar.cinema.repositores;

import com.belajar.cinema.models.Film;
import com.belajar.cinema.models.Genre;
import com.belajar.cinema.models.Negara;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoFilm extends JpaRepository<Film, Integer> {
    @Query(value = "SELECT * FROM film ORDER BY film ASC", nativeQuery = true)
    List<Film> FindAllGenreOrderByASC();

    @Query(value = "SELECT film.* FROM artis LEFT JOIN film ON artis.kd_artis = film.artis Where artis.kd_artis = :kode", nativeQuery = true)
    List<Film> joinLeftFilmByKdArtis(Integer kode);
    @Query(value = "SELECT film.* FROM genre LEFT JOIN film ON genre.kd_genre = film.genre Where genre.kd_genre = :kode", nativeQuery = true)
    List<Film> joinLeftFilmByKdGenre(Integer kode);
    @Query(value = "SELECT film.* FROM produser LEFT JOIN film ON produser.kd_produser = film.produser Where produser.kd_produser = :kode", nativeQuery = true)
    List<Film> joinLeftFilmByKdProduser(Integer kode);
    @Query(value = "SELECT film.* FROM negara LEFT JOIN artis ON artis.negara = negara.kd_negara INNER JOIN film ON artis.kd_artis = film.artis Where negara.kd_negara = :kode", nativeQuery = true)
    List<Film> joinLeftFilmByKdNegara(Integer kode);

}
