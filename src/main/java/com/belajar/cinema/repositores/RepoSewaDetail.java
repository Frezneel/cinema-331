package com.belajar.cinema.repositores;

import com.belajar.cinema.models.SewaDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoSewaDetail extends JpaRepository<SewaDetail, Long> {
    @Query(value = "SELECT * FROM sewa_detail where sewa_detail.id_header = :id_header", nativeQuery = true)
    List<SewaDetail> getAllDetailByIdHeader(Long id_header);
}
