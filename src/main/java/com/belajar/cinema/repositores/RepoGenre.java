package com.belajar.cinema.repositores;

import com.belajar.cinema.models.Genre;
import com.belajar.cinema.models.Negara;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepoGenre extends JpaRepository<Genre,Integer> {
    @Query(value = "SELECT * FROM genre ORDER BY genre ASC", nativeQuery = true)
    List<Genre> FindAllGenreOrderByASC();
}
