package com.belajar.cinema.repositores;

import com.belajar.cinema.models.Artis;
import com.belajar.cinema.models.Film;
import com.belajar.cinema.models.Negara;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.thymeleaf.expression.Lists;

import java.util.List;
import java.util.Optional;

public interface RepoArtis extends JpaRepository<Artis, Integer> {
    @Query(value = "SELECT * FROM artis ORDER BY artis ASC", nativeQuery = true)
    List<Artis> FindAllArtisOrderByASC();

}
